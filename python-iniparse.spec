Name:           python-iniparse
Version:        0.5
Release:        4
Summary:        Python Module for Accessing and Modifying Configuration Data in INI files
License:        MIT AND Python-2.0
URL:            https://github.com/candlepin/python-iniparse
Source0:        https://files.pythonhosted.org/packages/source/i/iniparse/iniparse-%{version}.tar.gz

Patch0: backport-fix-bug-in-classifiers.patch
Patch1: backport-Fix-compatibility-issues-with-Python-3.11.patch
Patch2: backport-Fix-tests-with-python-3.12.1.patch

BuildArch: noarch

%description
iniparse is an INI parser for Python which is API compatible
with the standard library's ConfigParser, preserves structure of INI
files (order of sections & options, indentation, comments, and blank
lines are preserved when data is updated), and is more convenient to
use.


%package -n python3-iniparse
Summary:        Python 3 version of python-iniparse
%{?python_provide:%python_provide python3-iniparse}
BuildRequires:  python3-devel python3-setuptools python3-six python3-test
Requires:       python3-six

%description -n python3-iniparse
This package is the python3 version of python-iniparse.

%package_help

%prep
%autosetup -n iniparse-%{version} -p1

%build
%py3_build

%install
%py3_install

rm -fr %{buildroot}%{_datadir}/doc

%check
export PYTHONPATH=%{buildroot}%{python3_sitelib}
%{__python3} runtests.py

%files -n python3-iniparse
%license LICENSE LICENSE-PSF
%{python3_sitelib}/iniparse
%{python3_sitelib}/iniparse-%{version}-py*.egg-info

%files help
%doc Changelog html/

%changelog
* Thu Jan 23 2025 Funda Wang <fundawang@yeah.net> - 0.5-4
- fix test with python 3.11

* Wed Aug 14 2024 wangjiang <wangjiang37@h-partners.com> - 0.5-3
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:License compliance rectification

* Tue Mar 26 2024 xiangyuning<xiangyuning@huawei.com> - 0.5-2
- ID:NA
- SUG:NA
- DESC:fix bug in classifiers

* Wed Jul 29 2020 tianwei<tianwei12@huawei.com> - 0.5-1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:update release to 0.5

* Fri Sep 27 2019 shenyangyang<shenyangyang4@huawei.com> - 0.4-36
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:modify license

* Sat Sep 21 2019 shenyangyang<shenyangyang4@huawei.com> - 0.4-35
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:revise patch

* Thu Sep 12 2019 shenyangyang<shenyangyang4@huawei.com> - 0.4-34
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:revise the comments of patch

* Wed Sep 11 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.4-33
- revise spec
* Sat Jun 12 2010 Paramjit Oberoi <param@cs.wisc.edu> - 0.4-1
- Release 0.4
* Sat Apr 17 2010 Paramjit Oberoi <param@cs.wisc.edu> - 0.3.2-1
- Release 0.3.2
* Mon Mar 2 2009 Paramjit Oberoi <param@cs.wisc.edu> - 0.3.1-1
- Release 0.3.1
* Fri Feb 27 2009 Paramjit Oberoi <param@cs.wisc.edu> - 0.3.0-1
- Release 0.3.0
* Sat Dec 6 2008 Paramjit Oberoi <param@cs.wisc.edu> - 0.2.4-1
- Release 0.2.4
- added egg-info file to %%files
* Tue Dec 11 2007 Paramjit Oberoi <param@cs.wisc.edu> - 0.2.3-1
- Release 0.2.3
* Mon Sep 24 2007 Paramjit Oberoi <param@cs.wisc.edu> - 0.2.2-1
- Release 0.2.2
* Tue Aug 7 2007 Paramjit Oberoi <param@cs.wisc.edu> - 0.2.1-1
- Release 0.2.1
* Fri Jul 27 2007 Tim Lauridsen <timlau@fedoraproject.org> - 0.2-3
- relocated doc to %{_docdir}/python-iniparse-%{version}
* Thu Jul 26 2007 Tim Lauridsen <timlau@fedoraproject.org> - 0.2-2
- changed name from iniparse to python-iniparse
* Tue Jul 17 2007 Tim Lauridsen <timlau@fedoraproject.org> - 0.2-1
- Release 0.2
- Added html/* to %%doc
* Fri Jul 13 2007 Tim Lauridsen <timlau@fedoraproject.org> - 0.1-1
- Initial build.
